#include<stdio.h>
#include<string.h>
struct MyPoint
{
	int x;
	int y;
};

struct MyPointProperties
{
	int quadrant;
	char axis_location[10];
};



int main(void)
{
	struct MyPoint point;
	struct MyPointProperties point_properties;
	printf("\n\n\n");
	printf("Enter X Co-Ordinate for A Point: ");
	scanf("%d",&point.x);

	printf("Enter Y-Coordinate For B point:  ");
	scanf("%d",&point.y);

	printf("\n\n");
	printf("Point Co-ordinates (x,y) are : (%d %d) !!!",point.x,point.y);

	if(point.x==0 && point.y==0)
		printf("The Point Is the Origin (%d ,%d) !! \n",point.x,point.y);
	else
	{
		if(point.x==0)
		{
			if(point.y<0)
				strcpy(point_properties.axis_location,"Negative Y");
			
			if(point.y>0)
			{
				strcpy(point_properties.axis_location,"Possitive Y");
			}
			point_properties.quadrant=0;
			printf("The Point Lies on the %s axis !!\n\n",point_properties.axis_location);
		}
		else if(point.y==0)
		{
			if(point.x<0)
				strcpy(point_properties.axis_location,"Negative X");
			if(point.x>0)
				strcpy(point_properties.axis_location,"Possitive X");

			point_properties.quadrant=0;
			printf("The Point Lies On the %s Axis !!\n\n",point_properties.axis_location);

		}
		else
		{
			point_properties.axis_location[0]='\0';

			if(point.x>0 && point.y>0)
				point_properties.quadrant=1;
			else if(point.x<0 && point.y>0)
				point_properties.quadrant=2;
			else if(point.x<0 && point.y<0)
				point_properties.quadrant=3;
			else
				point_properties.quadrant=4;

			printf("The Point Lies in Quadrant Number %d !!!\n\n",point_properties.quadrant);
		}
	}

	return 0;
}