#include<stdio.h>

struct MyData
{
	int i;
	float f;
	double d;
};

int main(void)
{
	struct MyData data;

	int i_size;
	int f_size;
	int d_size;
	int struct_MyData_size;

	//code
	data.i=30;
	data.f=11.45f;
	data.d=1.2354;

	//displaying Values of the Data Members of 'struct MyData'
	printf("\n\n");
	printf("i=%d\n",data.i);
	printf("f=%f\n",data.f);
	printf("d=%lf\n",data.d);

	//calculating size of data members
	i_size=sizeof(data.i);
	f_size=sizeof(data.f);
	d_size=sizeof(data.d);

	printf("\n\n");
	printf("SIZES (in Bytes) OF DATA MEMBERS OF struct Mydata are :\n\n");
	printf("Size of 'i'=%d bytes \n",i_size);
	printf("Size of 'f'=%d bytes \n",f_size);
	printf("Size of 'd'=%d bytes \n",d_size);

	//calculating size of entire struct MyData
	struct_MyData_size=sizeof(struct MyData);

	//displaying sizes of the entire struct Mydata
	printf("\n\n");
	printf("size of 'struct MyData': %d Bytes\n\n",struct_MyData_size);



	return 0;
}