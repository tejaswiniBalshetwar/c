#include<stdio.h>
//#include<string.h>

struct MyData
{
	int i;
	float f;
	double d;
	char c;
};

int main(void)
{
	struct MyData data;
	printf("\n\n");
	printf("Enter Integer value for Data Member i of struct MyData: \n");
	scanf("%d",&data.i);

	printf("Enter float value for Data Member f of struct MyData: \n");
	scanf("%f",&data.f);

	printf("Enter double value for Data Member d of struct MyData: \n");
	scanf("%lf",&data.d);

	printf("Enter charecter value for Data Member c of struct MyData: \n");
	data.c=getchar();



	printf("\n\n");
	printf("DATA MEMBERS OF 'struct MyData' ARE: \n\n");

	printf("i=%d\n",data.i);
	printf("f=%f\n",data.f);
	printf("d=%lf\n",data.d);
	printf("c=%c\n\n",data.c);
	

	return 0;
}