#include<stdio.h>

struct MyPoint
{
	int x;
	int y;
};


int main(void)
{
	struct MyPoint point_A,point_B,point_C,point_D,point_E;
	//code
	//Assigning Data Values to the data Members of 'struct MyPoint' variable 'point_A' 
	
	printf("\n\n");
	printf("Enter X Co-Ordinates for Point 'A': ");
	scanf("%d",&point_A.x);
	
	printf("Enter Y Co-Ordinates for Point 'A': ");
	scanf("%d",&point_A.y);

	printf("\n\n");
	printf("Enter X Co-Ordinates for Point 'B': ");
	scanf("%d",&point_B.x);
	
	printf("Enter Y Co-Ordinates for Point 'B': ");
	scanf("%d",&point_B.y);
		
	printf("\n\n");
	printf("Enter X Co-Ordinates for Point 'C'");
	scanf("%d",&point_C.x);
	
	printf("Enter Y Co-Ordinates for Point 'C': ");
	scanf("%d",&point_C.y);

	printf("\n\n");
	printf("Enter X Co-Ordinates for Point 'D': ");
	scanf("%d",&point_D.x);
	
	printf("Enter Y Co-Ordinates for Point 'D': ");
	scanf("%d",&point_D.y);

	printf("\n\n");
	printf("Enter X Co-Ordinates for Point 'E': ");
	scanf("%d",&point_E.x);
	
	printf("Enter Y Co-Ordinates for Point 'E': ");
	scanf("%d",&point_E.y);

	


	//Displaying Values of the data Members of Struct MyPoint (All variables)
	printf("\n\n");
	printf("Co-Ordinates (x,y) Of Point 'A' Are: (%d,%d)\n\n",point_A.x,point_A.y);
	printf("Co-Ordinates (x,y) Of Point 'B' Are: (%d,%d)\n\n",point_B.x,point_B.y);
	printf("Co-Ordinates (x,y) Of Point 'C' Are: (%d,%d)\n\n",point_C.x,point_C.y);
	printf("Co-Ordinates (x,y) Of Point 'D' Are: (%d,%d)\n\n",point_D.x,point_D.y);
	printf("Co-Ordinates (x,y) Of Point 'E' Are: (%d,%d)\n\n",point_E.x,point_E.y);


	return 0;
}