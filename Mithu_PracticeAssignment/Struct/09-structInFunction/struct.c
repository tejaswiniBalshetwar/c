#include<stdio.h>

struct MyData
{
	int i;
	float f;
	double d;
	char c;
};

int main(void)
{
	struct MyData data1,data2,data3,answer;
	struct MyData AddStructMembers(struct MyData,struct MyData,struct MyData);


	//code
	printf("\n\n\n\n");
	printf("***********DATA1 ***************\n\n");
	printf("Enter The integer value for i of struct data1: ");
	scanf("%d",&data1.i);
	
	printf("Enter The float value for f of struct data1: ");
	scanf("%f",&data1.f);
	
	printf("Enter The double value for d of struct data1: ");
	scanf("%lf",&data1.d);
	
	printf("Enter The char value for c of struct data1: ");
	data1.c=getchar();
	printf("%c",data1.c);

	printf("\n\n\n\n");
	printf("***********DATA2 ***************\n\n");
	printf("Enter The integer value for i of struct data2: ");
	scanf("%d",&data2.i);
	
	printf("Enter The float value for f of struct data2: ");
	scanf("%f",&data2.f);
	
	printf("Enter The double value for d of struct data2: ");
	scanf("%lf",&data2.d);
	
	printf("Enter The char value for c of struct data2: ");
	data2.c=getchar();
	printf("%c",data2.c);

	printf("\n\n\n\n");
	printf("***********DATA3 ***************\n\n");
	printf("Enter The integer value for i of struct data3 : ");
	scanf("%d",&data3.i);
	
	printf("Enter The float value for f of struct data3: ");
	scanf("%f",&data3.f);
	
	printf("Enter The double value for d of struct data3: ");
	scanf("%lf",&data3.d);
	
	printf("Enter The char value for c of struct data3: ");
	data3.c=getchar();
	printf("%c",data3.c);

	answer=AddStructMembers(data1,data2,data3);

	printf("\n\n");
	printf("**************ANSWER *************: \n\n");

	printf("answer i=%d\n",answer.i);
	printf("answer f=%f\n",answer.f);
	printf("answer d=%lf\n",answer.d);
	answer.c=data1.c;
	printf("answer c from data1=%c\n\n",answer.c);

	answer.c=data2.c;
	printf("answer c from data2=%c\n\n",answer.c);

	answer.c=data3.c;
	printf("answer c from data3=%c\n\n",answer.c);

	return 0;
}

struct MyData AddStructMembers(struct MyData md_one,struct MyData md_two,struct MyData md_three)
{
	struct MyData answer;


	answer.i=md_one.i+md_two.i+md_three.i;
	answer.f=md_one.f+md_two.f+md_three.f;
	answer.d=md_one.d+md_two.d+md_three.d;
	return (answer);
}