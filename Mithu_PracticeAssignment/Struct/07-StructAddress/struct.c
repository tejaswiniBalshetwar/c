#include<stdio.h>

struct MyData
{
	int i;
	float f;
	double d;
	char c;
};

int main(void)
{
	struct MyData data_one ={30,4.5f,11.25634,'A'};
	printf("\n\n");
	printf("DATA MEMBERS OF 'struct MyData' ARE: \n\n");

	printf("i=%d\n",data_one.i);
	printf("f=%f\n",data_one.f);
	printf("d=%lf\n",data_one.d);
	printf("c=%c\n\n",data_one.c);


	printf("\n\n");
	printf("ADDRESS OF DATA MEMBERS OF 'struct MyData' ARE: \n\n");

	printf("i occupies address from %p\n",&data_one.i);
	printf("f occupies address from %p\n",&data_one.f);
	printf("d occupies address from %p\n",&data_one.d);
	printf("c occupies address %p\n\n\n",&data_one.c);

	
	return 0;
}