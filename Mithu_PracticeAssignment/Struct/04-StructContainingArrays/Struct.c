#include<stdio.h>
#include<string.h>

#define INT_ARRAY_SIZE 10
#define FLOAT_ARRAY_SIZE 5
#define CHAR_ARRAY_SIZE 26

#define NUM_STRINGS 10
#define MX_CHARACTERS_PER_STRING 20
#define ALPHABET_BEGINNING 65

struct MyDataOne
{
	int iArray[INT_ARRAY_SIZE];
	float fArray[FLOAT_ARRAY_SIZE];
	
};

struct MyDataTwo
{	
	char cArray[CHAR_ARRAY_SIZE];
	char strArray[NUM_STRINGS][MX_CHARACTERS_PER_STRING];
	
};

int main(void)
{
	struct MyDataOne data_one;
	struct MyDataTwo data_two;
	int i;

	//piece mill assignment
	data_one.fArray[0]=0.1f;
	data_one.fArray[1]=1.2f;
	data_one.fArray[2]=2.3f;
	data_one.fArray[3]=3.4f;
	data_one.fArray[4]=4.5f;

	//Loop Assignment(USER INPUT)
	printf("\n\n");
	printf("Enter %d integers : \n\n",INT_ARRAY_SIZE );
	for(i=0;i<INT_ARRAY_SIZE;i++)
	{
		scanf("%d",&data_one.iArray[i]);
	}

	//loop assignment HARD-coded
	for(i=0;i<CHAR_ARRAY_SIZE;i++)
	{
		data_two.cArray[i]=(char)(i+ALPHABET_BEGINNING);
	}

	//piecemeal assignment 
	strcpy(data_two.strArray[0],"Welcome!!!");
	strcpy(data_two.strArray[1],"This");
	strcpy(data_two.strArray[2],"is ");
	strcpy(data_two.strArray[3],"Astomedicomp's");
	strcpy(data_two.strArray[4],"real");
	strcpy(data_two.strArray[5],"time");
	strcpy(data_two.strArray[6],"rendering");
	strcpy(data_two.strArray[7],"batch");
	strcpy(data_two.strArray[8],"of");
	strcpy(data_two.strArray[9],"2018-19-20!!!");

	printf("\n\n");

	printf("Members of struct 'struct MyDataOne along with values are:\n");

	printf("\n\n");
	for(i=0;i<INT_ARRAY_SIZE;i++)
	{
		printf("data_one.iArray[%d]=%d\n",i,data_one.iArray[i]);
	}


	printf("\n\n");
	for(i=0;i<FLOAT_ARRAY_SIZE;i++)
	{
		printf("data_one.fArray[%d]=%f\n",i,data_one.fArray[i]);
	}
	
	printf("\n\n");

	printf("Members of struct 'struct MyDataTwo' along with values are:\n");

	printf("\n\n");
	for(i=0;i<CHAR_ARRAY_SIZE;i++)
	{
		printf("data_two.cArray[%d]=%c\n",i,data_two.cArray[i]);
	}


	printf("\n\n");
	printf("String Array (data_two.strArray[]): \n\n");
	for(i=0;i<NUM_STRINGS;i++)
	{
		printf("%s ",data_two.strArray[i]);
	}
	

	printf("\n\n");

	return 0;
}