#include<stdio.h>

struct MyData
{
	int i;
	float f;
	double d;
	char c;
};

int main(void)
{
	struct MyData data_one ={30,4.5f,11.25634,'A'};
	printf("\n\n");
	printf("DATA MEMBERS OF 'struct MyData' ARE: \n\n");

	printf("i=%d\n",data_one.i);
	printf("f=%f\n",data_one.f);
	printf("d=%lf\n",data_one.d);
	printf("c=%c\n\n",data_one.c);


	struct MyData data_two ={'P',4.5f,10.25634,68};
	printf("\n\n");
	printf("DATA MEMBERS OF 'struct MyData' ARE: \n\n");

	printf("i=%d\n",data_two.i);
	printf("f=%f\n",data_two.f);
	printf("d=%lf\n",data_two.d);
	printf("c=%c\n\n",data_two.c);
	

	struct MyData data_three ={36,'G'};
	printf("\n\n");
	printf("DATA MEMBERS OF 'struct MyData' ARE: \n\n");

	printf("i=%d\n",data_three.i);
	printf("f=%f\n",data_three.f);
	printf("d=%lf\n",data_three.d);
	printf("c=%c\n\n",data_three.c);

	struct MyData data_four ={79};
	printf("\n\n");
	printf("DATA MEMBERS OF 'struct MyData' ARE: \n\n");

	printf("i=%d\n",data_four.i);
	printf("f=%f\n",data_four.f);
	printf("d=%lf\n",data_four.d);
	printf("c=%c\n\n",data_four.c);

	return 0;
}