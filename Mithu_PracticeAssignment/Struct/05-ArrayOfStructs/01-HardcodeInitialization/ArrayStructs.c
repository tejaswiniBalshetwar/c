#include<stdio.h>
#include<string.h>

#define NAME_LENGTH 100
#define MARITAL_STATUS 10

struct Employee
{
	char name[NAME_LENGTH];
	int age;
	float salary;
	char sex;
	char marital_status[MARITAL_STATUS];

};


int main(int argc, char const *argv[])
{
	struct Employee  EmployeeRecord[5];

	char employee_ankur[]="Ankur";
	char employee_tejaswini[]="Tejaswini";
	char employee_chetan[]= "Chetan";
	char employee_ram[]="Ram";
	char employee_tushar[]="Tushar";

	int i;

	//Employee record 1
	strcpy(EmployeeRecord[0].name,employee_ankur);
	EmployeeRecord[0].age=30;
	EmployeeRecord[0].sex='M';
	EmployeeRecord[0].salary=600000.0f;
	strcpy(EmployeeRecord[0].marital_status,"UnMarried");

	strcpy(EmployeeRecord[1].name,employee_tejaswini);
	EmployeeRecord[1].age=24;
	EmployeeRecord[1].sex='F';
	EmployeeRecord[1].salary=22000.0f;
	strcpy(EmployeeRecord[1].marital_status,"UnMarried");

	strcpy(EmployeeRecord[2].name,employee_chetan);
	EmployeeRecord[2].age=29;
	EmployeeRecord[2].sex='M';
	EmployeeRecord[2].salary=30000.0f;
	strcpy(EmployeeRecord[2].marital_status,"UnMarried");

	strcpy(EmployeeRecord[3].name,employee_ram);
	EmployeeRecord[3].age=35;
	EmployeeRecord[3].sex='M';
	EmployeeRecord[3].salary=50000.0f;
	strcpy(EmployeeRecord[3].marital_status,"Married");

	strcpy(EmployeeRecord[4].name,employee_tushar);
	EmployeeRecord[4].age=25;
	EmployeeRecord[4].sex='M';
	EmployeeRecord[4].salary=25000.0f;
	strcpy(EmployeeRecord[4].marital_status,"UnMarried");


	//display the records
	printf("\n\n");
	printf("**********DIPLAYNG EMPLOYEE RECORDS\n\n");
	for(i=0;i<5;i++)	
	{
		printf("************ EMPLOYEE NUMBER %d ************\n\n",(i+1));
		printf("name 			:%s\n",EmployeeRecord[i].name);
		printf("age 			:%d\n",EmployeeRecord[i].age);
		if(EmployeeRecord[i].sex=='M' || EmployeeRecord[i].sex=='m')
		{
			printf("Sex 		:Male\n");
		}
		else
		{
			printf("Sex 		:Female\n");
		}
		printf("salary			:Rs. %f\n",EmployeeRecord[i].salary);
		printf("Marital statis			: %s\n",EmployeeRecord[i].marital_status);
		
		printf("\n\n");
	}



	return 0;
}