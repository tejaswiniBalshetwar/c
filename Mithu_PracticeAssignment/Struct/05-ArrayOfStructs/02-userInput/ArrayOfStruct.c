#include<stdio.h>
#include<ctype.h>

#define NUM_EMPLOYEES 5

#define NAME_LENGTH 100
#define MARITAL_STATUS 10

struct Employee
{
	char name[NAME_LENGTH];
	int age;
	float salary;
	char sex;
	char marital_status;

};


int main(int argc, char const *argv[])
{
	void MyGetString(char [],int);
	struct Employee  EmployeeRecord[NUM_EMPLOYEES];

	int i;

	//code
	for(i=0;i<NUM_EMPLOYEES;i++)
	{
		printf("\n\n\n\n");
		printf("********Data Entry for Employee Number %d ***********\n",(i+1));
		printf("\n\n");
		printf("Enter Employee Name: ");
		MyGetString(EmployeeRecord[i].name,NAME_LENGTH);

		printf("\n\n");
		printf("Enter Employee age: ");
		scanf("%d",&EmployeeRecord[i].age);
		
		printf("\n\n");
		printf("Is  Employee's sex  (M/m for Male and F/f for female): ");
		EmployeeRecord[i].sex=getchar();
		printf("%c",EmployeeRecord[i].sex);
		EmployeeRecord[i].sex=toupper(EmployeeRecord[i].sex);
		
		printf("\n\n");
		printf("Is  Employee Married (Y/y for yes and N/n for No): ");
		EmployeeRecord[i].marital_status=getchar();
		printf("%c",EmployeeRecord[i].marital_status);
		EmployeeRecord[i].marital_status=toupper(EmployeeRecord[i].marital_status);
		
	}


	//display the records
	printf("\n\n");
	printf("**********DIPLAYNG EMPLOYEE RECORDS\n\n");
	for(i=0;i<5;i++)	
	{
		printf("************ EMPLOYEE NUMBER %d ************\n\n",(i+1));
		printf("name 			:%s\n",EmployeeRecord[i].name);
		printf("age 			:%d years\n",EmployeeRecord[i].age);
		if(EmployeeRecord[i].sex=='M' )
		{
			printf("Sex 		:Male\n");
		}
		else
		{
			printf("Sex 		:Female\n");
		}
		printf("salary			:Rs. %f\n",EmployeeRecord[i].salary);
		
		if(EmployeeRecord[i].marital_status=='Y' )
		{
			printf("MARITAL STATUS 		:Married\n");
		}
		else if(EmployeeRecord[i].marital_status=='N' )
		{
			printf("MARITAL STATUS 		:UnMarried\n");
		}
		else
		{
			printf("Invalid Data\n");
		}
		
		
		printf("\n\n");
	}



	return 0;
}


void MyGetString(char name[],int str_size)
{
	int i;
	char ch ='\0';

	//code
	i=0;
	do
	{
		ch=getchar();
		name[i]=ch;
		printf("%c",name[i]);
		i++;
	}
	while((ch!='\r')&&(i<=str_size));

	if(i==str_size)
	{
		name[i-1]='\0';
	}
	else
		name[i]='\0';

}