#include<stdio.h>

union MyUnion
{
	int i;
	float f;
	double d;
	char c;
};

struct MyStruct
{
	int i;
	float f;
	double d;
	char c;
};

int main()
{
	union MyUnion u;
	struct MyStruct s;

	//code
	printf("\n\n");
	printf("Members of struct s are: \n\n");
	s.i=5;
	s.f=10.2367f;
	s.d=8.54325;
	s.c='S';

	printf("\n\n");
	printf("s.i=%d\n",s.i);
	printf("s.f=%f\n",s.f);
	printf("s.d=%lf\n",s.d);
	printf("s.c=%c\n",s.c);

	printf("\n\n");
	printf("address of s.i=%p\n",&s.i);
	printf("address of s.f=%p\n",&s.f);
	printf("address of s.d=%p\n",&s.d);
	printf("address of s.c=%p\n",&s.c);

	printf("MyStruct s=%p\n\n",&s);



	printf("\n\n");
	printf("Members of Union u are: \n\n");
	
	u.i=6;
	printf("u.i=%d\n",u.i);
	
	u.f=12.2367f;
	printf("u.f=%f\n",u.f);
	
	u.d=18.54325;
	printf("u.d=%lf\n",u.d);
	
	u.c='U';

	printf("u.c=%c\n",u.c);

	printf("\n\n");
	printf("address of u.i=%p\n",&u.i);
	printf("address of u.f=%p\n",&u.f);
	printf("address of u.d=%p\n",&u.d);
	printf("address of u.c=%p\n",&u.c);

	printf("MyUnion u2=%p\n\n",&u);


	return 0;
}