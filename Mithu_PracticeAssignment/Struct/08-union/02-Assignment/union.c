#include<stdio.h>

union MyUnion
{
	int i;
	float f;
	double d;
	char c;
};

int main()
{
	union MyUnion u1,u2;

	//code
	printf("\n\n");
	printf("Members of Union u1 are: \n\n");
	u1.i=5;
	u1.f=10.2367f;
	u1.d=8.54325;
	u1.c='S';

	printf("\n\n");
	printf("u1.i=%d\n",u1.i);
	printf("u1.f=%f\n",u1.f);
	printf("u1.d=%lf\n",u1.d);
	printf("u1.c=%c\n",u1.c);

	printf("\n\n");
	printf("address of u1.i=%p\n",&u1.i);
	printf("address of u1.f=%p\n",&u1.f);
	printf("address of u1.d=%p\n",&u1.d);
	printf("address of u1.c=%p\n",&u1.c);

	printf("MyUnion u1=%p\n\n",&u1);



	printf("\n\n");
	printf("Members of Union u2 are: \n\n");
	
	u2.i=6;
	printf("u2.i=%d\n",u2.i);
	
	u2.f=12.2367f;
	printf("u2.f=%f\n",u2.f);
	
	u2.d=18.54325;
	printf("u2.d=%lf\n",u2.d);
	
	u2.c='F';

	printf("u2.c=%c\n",u2.c);

	printf("\n\n");
	printf("address of u2.i=%p\n",&u2.i);
	printf("address of u2.f=%p\n",&u2.f);
	printf("address of u2.d=%p\n",&u2.d);
	printf("address of u2.c=%p\n",&u2.c);

	printf("MyUnion u2=%p\n\n",&u2);
	



	return 0;
}