#include<stdio.h>

union MyUnion
{
	int i;
	float f;
	double d;
	char c;
};

struct MyStruct
{
	int i;
	float f;
	double d;
	char c;
};

int main()
{
	union MyUnion u;
	struct MyStruct s;

	//code
	printf("\n\n");
	printf("Size of MyStruct = %ld\n",sizeof(s));

	printf("\n\n");
	printf("Size of MyUnion = %ld\n",sizeof(u));
	
	return 0;
}