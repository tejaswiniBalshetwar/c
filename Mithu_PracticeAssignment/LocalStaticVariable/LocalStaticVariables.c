#include<stdio.h>

int main(void)
{
	int a=5;
	void ChangeCount(void);

	//code
	printf("\n");
	printf("A=%d\n",a);

	//Initialized to 1  and the value is retained by the variable
	ChangeCount(); //1

	//ChangeCount() will make it local_Count=Local_Count+1=2
	ChangeCount();

	//change to 3
	ChangeCount();


	return 0;
}

void ChangeCount(void)
{
	static int local_Count=0;

	local_Count=local_Count+1;
	printf("static Declared Local_Count= %d\n",local_Count);
}

