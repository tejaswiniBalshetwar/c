#include<stdio.h>

int main()
{
	int a;
	int b;
	int result;

	printf("\n\n Enter first Number : \n\n");
	scanf("%d",&a);

	printf("\n\n Enter second  Number : \n\n");
	scanf("%d",&b);

	printf("\n\n if result = 0 then It is FALSE\n");
	printf("\n\n if result = 1 then It is TRUE\n\n\n");


	result = (a<b);
	printf("(a<b) A=%d is less than b=%d 	 				\t Answer = %d \n\n",a,b,result);

	result = (a>b);
	printf("(a > b) A=%d is greater than b=%d 				\t Answer = %d \n\n",a,b,result);

	result = (a<=b);
	printf("(a <= b) A=%d is less than and equal to b=%d 			\t Answer = %d \n\n",a,b,result);

	result = (a>=b);
	printf("(a >= b) A=%d is greater than equal to b=%d 			\t Answer = %d \n\n",a,b,result);

	result = (a==b);
	printf("(a == b) A=%d is equal to b=%d 					\t Answer = %d \n\n",a,b,result);

	result = (a != b);
	printf("(a != b) A=%d is not equal b=%d 					\t Answer = %d \n\n",a,b,result);


	return 0;
}