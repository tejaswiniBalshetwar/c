#include<stdio.h>

int main(void)
{

	void PrintBinaryFormOfNumber(unsigned int);

	unsigned int a;
	unsigned int b;
	unsigned int result;

	printf("\n\n");
	printf("Enter the Integer=");
	scanf("%d",&a);

	printf("\n\n");
	printf("Enter Another Integer=");
	scanf("%d",&b);

	printf("\n\n");
	result=a & b;
	printf("Bitwise AND-ing of \nA=%d (decimal),  %o (octal) ,  %X(HexaDecimal) and \nB=%d(Decimal) , %o(Octal), %X(HexaDecimal) \n Gives the result= %d (decimal) , %o(octal) ,%X(HexaDecimal).\n \n",a,a,a,b,b,b,result,result,result);

	PrintBinaryFormOfNumber(a);
	PrintBinaryFormOfNumber(b);
	PrintBinaryFormOfNumber(result);	
	


	return 0;
}

void PrintBinaryFormOfNumber(unsigned int decimal_number)
{
	unsigned int quotient,remainder;
	unsigned int num;
	unsigned int binary_number[8];
	int i;

	for(i=0;i<8;i++)
	{
		binary_number[i]=0;

	}

	printf("\n\n Binary Form of Decimal Number %d = \t",decimal_number);
	num=decimal_number;
	i=7;

	while(num!=0)
	{
		quotient=num/2;
		remainder=num%2;
		binary_number[i]=remainder;
		num=quotient;
		i--;
	}

	for(i=0;i<8;i++)
		printf("%u",binary_number[i]);

	printf("\n\n");
}