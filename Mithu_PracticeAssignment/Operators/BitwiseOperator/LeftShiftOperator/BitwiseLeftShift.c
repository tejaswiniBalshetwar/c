
#include<stdio.h>

int main(void)
{
	void PrintBinaryFormDecimal(unsigned int );

	unsigned int a,b , result;
	unsigned int left_shift_A,left_shift_B;


	printf("\n\n");
	printf("Enter An Integer:" );
	scanf("%u",&a);

	printf("\n\n");
	printf("Enter Second Integer:");
	scanf("%u",&b);

	printf("\n\n");
	printf("Enter how many bits you want to shift A:");
	scanf("%u",&left_shift_A);
	
	printf("\n\n");
	printf("Enter how many bits you want to shift B:");
	scanf("%u",&left_shift_B);

	printf("\n\n\n\n");
	result=a<<left_shift_A;

	printf("BitWise Left Shift By %d bits of \n A=%d (decimal) , %o (Octal) , %X (HexaDecimal)\n\n result is = %d (Decimal) , %o(Octal) ,%X(HexaDecimal) \n\n",left_shift_A,a,a,a,result,result,result );

	PrintBinaryFormDecimal(a);
	PrintBinaryFormDecimal(result);
	
	printf("\n\n\n\n");
	result=b<<left_shift_B;

	printf(" BitWise LEFT Shifted By %d  bits of \nB=%d (Decimal) ,%o (Octal ), %X (HexaDecimal) \n\nresult is = %d (Decimal) , %o(Octal) ,%X(HexaDecimal) \n\n",left_shift_B,b,b,b,result,result,result);

	
	PrintBinaryFormDecimal(b);
	PrintBinaryFormDecimal(result);

	


	return 0;
}


void PrintBinaryFormDecimal(unsigned int decimal_number)
{

	unsigned int remainder,quotiant,num;
	unsigned int binary_number[8];
	int i;

	for(i=0;i<8;i++)
	{
		binary_number[i]=0;
	}

	printf("The binary Form of the Decimal Number %d is= \t ",decimal_number);
	num=decimal_number;

	i=7;
	while(num!=0)
	{
		quotiant=num/2;
		remainder=num%2;
		binary_number[i]=remainder;
		num=quotiant;
		i--;
	}

	for(i=0;i<8;i++)
		printf("%u",binary_number[i]);

	printf("\n\n");

}
