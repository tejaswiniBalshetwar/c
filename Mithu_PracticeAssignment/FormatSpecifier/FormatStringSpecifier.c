#include<stdio.h>

int main(void)
{
	printf("\n");
	printf("Hello World !!!\n\n\n");

	int a=13;
	printf("Integer Decimal Value of A: %d\n\n",a);
	printf("Integer Octal Value of A : %o\n\n",a);
	printf("Integer Hexadecimal Value of A(In Lower Case Letters): %x\n\n",a);
	printf("Integer Hexadecimal Value of A (In Upper Case Letters): %X\n\n",a);


	char ch='T';
	printf("Charecter Value of ch=%c \n\n",ch);
	char name_string[]="Tejaswini Harishchandra Balshetwar";
	printf("Charecter string Value : %s\n\n",name_string );

	long Number=12345645L;
	printf("Long Integer number=%ld\n\n",Number);

	unsigned int num=8;
	printf("unsigned int num= %u\n\n",num);

	float fNum=3012.45623f;
	printf("Float point Number with width %%f 'fNum':%f\n",fNum);
	printf("Float point Number with width %%4.2f 'fNum':%4.2f\n",fNum);	
	printf("Float point Number with width %%2.5f 'fNum':%2.5f\n\n",fNum);
	
	double dPI=3.14159265358979323846;
	printf("double precision floating point Number without Exponential : %g \n\n",dPI);
	printf("double precision floating point Number with Exponential(Lower Case) : %e \n\n",dPI);
	printf("double precision floating point Number with Exponential(UpperCase) : %E \n\n",dPI);

	printf("Double Hexadecimal value of 'dPI(Hexadecimal value in lower case):%a\n\n",dPI);
	printf("Double Hexadecimal value of 'dPI(Hexadecimal value in Upper case):%A\n\n",dPI);
	
	return 0;
}