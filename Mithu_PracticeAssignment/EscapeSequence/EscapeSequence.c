#include<stdio.h>

int main(void)
{
	printf("\n");

	printf("going to new line using \\n escape sequence \n\n");
	printf("Demonstrating \t  Horizonatal tab \t sequence using \\t escape sequence\n\n");
	printf("this is double quoted output\" done using \\\" \\\" escape sequence \n\n");
	printf("this is single quoted output \' done using \\\'  \\\' escape sequence\n\n");
	printf("BACKSPACE turned to BACKSPACE\b using escape sequence \\b\n\n");
	printf("\r explaning Carriage return using \\r escape sequence\n\n");
	printf("explaning \r Carriage return using \\r escape sequence\n");
	printf("explaning Carriage  \r return using \\r escape sequence\n\n");

	printf("Demonstrating \x41  using \\xhh escape sequence\n\n");
	printf("Demonstrating \102 using \\ooo escape sequence\n\n");

	return 0;
}