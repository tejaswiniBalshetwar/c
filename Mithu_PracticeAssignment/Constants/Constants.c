#include<stdio.h>

#define MY_PI 3.1415926535897932

enum
{
	SUNDAY,
	MONDAY,
	TUESDAY,
	WEDNESDAY,
	THURSDAY,
	FRIDAY,
	SATURDAY
};

enum
{
	JANUARY=1,
	FEBRUARY,
	MARCH,
	APRIL,
	MAY,
	JUNE,
	JULY,
	AUGUST,
	SEPTEMBER,
	OCTOMBER,
	NOVEMBER,
	DECEMBER
};

enum Numbers
{
	ONE,
	TWO,
	THREE,
	FOUR,
	FIVE=5,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN
};

enum boolean
{
	TRUE=1,
	FALSE=0
};

int main(void)
{
	const double epsilon=0.000001;
	printf("\n\n");

	printf("Local constant epsilon=%lf\n\n",epsilon);

	printf("SUNDAY is Day=%d\n",SUNDAY);
	printf("MONDAY is Day=%d\n",MONDAY);
	printf("TUESDAY is Day=%d\n",TUESDAY);
	printf("WEDNESDAY is Day=%d\n",WEDNESDAY);
	printf("THURSDAY is Day=%d\n",THURSDAY);
	printf("FRIDAY is Day=%d\n",FRIDAY);
	printf("SATURDAY is Day=%d\n\n",SATURDAY);

	printf("One is enum Number=%d\n",ONE);
	printf("Two is enum Number=%d\n",TWO);
	printf("Three is enum Number=%d\n",THREE);
	printf("Four is enum Number=%d\n",FOUR);
	printf("Five is enum Number=%d\n",FIVE);
	printf("Six is enum Number=%d\n",SIX);
	printf("Seven is enum Number=%d\n",SEVEN);
	printf("Eight is enum Number=%d\n",EIGHT);
	printf("Nine is enum Number=%d\n",NINE);
	printf("Ten is enum Number=%d\n\n\n",TEN);


	printf("January Is Month Number =%d\n",JANUARY);
	printf("February Is Month Number =%d\n",FEBRUARY);
	printf("March Is Month Number =%d\n",MARCH);
	printf("April Is Month Number =%d\n",APRIL);
	printf("May Is Month Number =%d\n",MAY);
	printf("June Is Month Number =%d\n",JUNE);
	printf("July Is Month Number =%d\n",JULY);
	printf("August Is Month Number =%d\n",AUGUST);
	printf("September Is Month Number =%d\n",SEPTEMBER);
	printf("Octomber Is Month Number =%d\n",OCTOMBER);
	printf("November Is Month Number =%d\n",NOVEMBER);
	printf("December Is Month Number =%d\n\n\n",DECEMBER);



	printf("Value of True = %d\n",TRUE);
	printf("Value of False=%d\n\n",FALSE);


	printf("MY_PI Micro Value is= %.10lf\n\n",MY_PI);
	printf("Area Of Circle of Radious 2 units is = %f \n\n",(MY_PI*2.0f*2.0f));
	return 0;
}