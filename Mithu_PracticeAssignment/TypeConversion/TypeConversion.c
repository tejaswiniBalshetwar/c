#include<stdio.h>

int main(void)
{
	int i ,j;
	char ch_01, ch_02;

	int a ,result_int;
	float f,result_f;

	int i_explicit;
	float f_explicit;


	i=70;
	ch_01=i;

	printf("I=%d\n",i);
	printf("charecter 1 (afeter ch_01=i)= %c\n\n",ch_01);

	ch_02='Q';
	j=ch_02;
	printf("charecter 2= %c \n",ch_02);
	printf("J (after j=ch_02)= %d\n\n",j);

	//implicit conversion
	a=5;
	f=7.8f;
	result_f=a+f;
	printf("Integer a= %d  and floating-point Number %f addeed gives Floating-Point Sum =%f\n\n",a,f,result_f);

	result_int=a+f;
	printf("Interger a=%d anf Floating Point Number %f added gives Integer Sum= %d\n\n",a,f,result_int);

	f_explicit=30.121995f;
	i_explicit=(int)f_explicit;
	printf("Floating Point Number Type Casted Explicitly=%f\n",f_explicit);
	printf("Resultant Integer After Explicit Type Casting of %f = %d \n\n",f_explicit,i_explicit);
	

	return 0;
}