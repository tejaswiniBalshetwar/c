#include<stdio.h>

#define MAX_STRING_LENGTH 512


int main()
{
	//function Prototype
	int MyStrlen(char[]);
	void MyStrCopy(char [],char []);
	//variable declaration


	char strArray[5][11];
	int char_size;
	int strArray_size;
	int strArray_num_elements,strArray_num_rows,strArray_num_columns;
	int strActual_num_chars=0;
	int i;

	//code

	printf("\n\n");

	char_size=sizeof(char);
	
	strArray_size=sizeof(strArray);
	printf("\nSize of the string Array = %d",strArray_size);

	strArray_num_rows=strArray_size/sizeof(strArray[0]);
	printf("\nThe number of rows of 2D stringArray = %d",strArray_num_rows);

	strArray_num_columns=sizeof(strArray[0])/char_size;
	printf("\n the number of columns in the string Array=%d",strArray_num_columns);	

	strArray_num_elements=strArray_num_columns*strArray_num_rows;
	printf("\nNumber of Elements in 2D String Array are= %d ",strArray_num_elements);


	//pieceMill initialization char by char

	strArray[0][0]='M';
	strArray[0][1]='Y';
	strArray[0][2]='\0';

	strArray[1][0]='N';
	strArray[1][1]='A';
	strArray[1][2]='M';
	strArray[1][3]='E';
	strArray[1][4]='\0';

	strArray[2][0]='I';
	strArray[2][1]='S';
	strArray[2][2]='\0';


	
	strArray[3][0]='T';
	strArray[3][1]='E';
	strArray[3][2]='J';
	strArray[3][3]='A';
	strArray[3][4]='S';
	strArray[3][5]='W';
	strArray[3][6]='I';
	strArray[3][7]='N';
	strArray[3][8]='I';	
	strArray[3][9]='\0';

	strArray[4][0]='B';
	strArray[4][1]='A';
	strArray[4][2]='L';
	strArray[4][3]='S';
	strArray[4][4]='H';
	strArray[4][5]='E';
	strArray[4][6]='T';
	strArray[4][7]='W';
	strArray[4][8]='A';	
	strArray[4][9]='R';
	strArray[4][10]='\0';
	


	printf("\n\nThe Actual string is\n\n");
	for(i=0;i<strArray_num_rows;i++)
	{
		printf("%s ",strArray[i]);
	}
	
	printf("\n\n");


	return 0;
}


void MyStrCopy(char destination[],char source[])
{
	int MyStrlen(char[]);
	int string_length=MyStrlen(source);
	int i;
	for(i=0;i<string_length;i++)
	{
		destination[i]=source[i];
	}
	destination[i]='\0';
}

int MyStrlen(char str[])
{
	int j;
	int string_length=0;

	for(j=0;j<MAX_STRING_LENGTH;j++)
	{
		if(str[j]=='\0')
		{
			break;
		}
		else
		{
			string_length++;
		}
	}

	return (string_length);
} 