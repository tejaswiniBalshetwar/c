#include<stdio.h>

#define MAX_STRING_LENGTH 512


int main()
{
	//function Prototype
	int MyStrlen(char[]);
	void MyStrCopy(char [],char []);
	//variable declaration


	char strArray[5][11];
	int char_size;
	int strArray_size;
	int strArray_num_elements,strArray_num_rows,strArray_num_columns;
	int strActual_num_chars=0;
	int i;

	//code

	printf("\n\n");

	char_size=sizeof(char);
	
	strArray_size=sizeof(strArray);
	printf("\nSize of the string Array = %d",strArray_size);

	strArray_num_rows=strArray_size/sizeof(strArray[0]);
	printf("\nThe number of rows of 2D stringArray = %d",strArray_num_rows);

	strArray_num_columns=sizeof(strArray[0])/char_size;
	printf("\n the number of columns in the string Array=%d",strArray_num_columns);	

	strArray_num_elements=strArray_num_columns*strArray_num_rows;
	printf("\nNumber of Elements in 2D String Array are= %d ",strArray_num_elements);




	MyStrCopy(strArray[0],"My");
	MyStrCopy(strArray[1],"Name");
	MyStrCopy(strArray[2],"Is");
	MyStrCopy(strArray[3],"Tejaswini");
	MyStrCopy(strArray[4],"Balshetwar");


	printf("\n\nThe Actual string is\n\n");
	for(i=0;i<strArray_num_rows;i++)
	{
		printf("%s ",strArray[i]);
	}
	
	printf("\n\n");


	return 0;
}


void MyStrCopy(char destination[],char source[])
{
	int MyStrlen(char[]);
	int string_length=MyStrlen(source);
	int i;
	for(i=0;i<string_length;i++)
	{
		destination[i]=source[i];
	}
	destination[i]='\0';
}

int MyStrlen(char str[])
{
	int j;
	int string_length=0;

	for(j=0;j<MAX_STRING_LENGTH;j++)
	{
		if(str[j]=='\0')
		{
			break;
		}
		else
		{
			string_length++;
		}
	}

	return (string_length);
}