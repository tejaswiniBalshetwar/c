#include<stdio.h>

int main()
{

	int iArray[3][5];
	int int_size;
	int iArray_size;
	int iArray_num_elements,iArray_num_rows,iArray_num_columns;

	int i,j;

	//code

	printf("\n\n");

	int_size=sizeof(int);

	iArray_size=sizeof(iArray);
	printf("\n\nThe Size of 2D array is =%d",iArray_size);


	iArray_num_rows=iArray_size/sizeof(iArray[0]);
	printf("\n\nThe number of rows in 2D array iArray= %d",iArray_num_rows);


	iArray_num_columns=sizeof(iArray[0])/int_size;
	printf("\nThe number of Columns in 2D array iArray=%d\n",iArray_num_columns);

	iArray_num_elements=iArray_num_columns*iArray_num_rows;
	printf("\n\n the number of Elements in the 2D array are= %d",iArray_num_elements);



	//piece mill initialization

	//row 1
	iArray[0][0]=1;
	iArray[0][1]=2;
	iArray[0][2]=3;
	iArray[0][3]=4;
	iArray[0][4]=5;

	iArray[1][0]=6;
	iArray[1][1]=7;
	iArray[1][2]=8;
	iArray[1][3]=9;
	iArray[1][4]=10;

	iArray[2][0]=10;
	iArray[2][1]=12;
	iArray[2][2]=13;
	iArray[2][3]=14;
	iArray[2][4]=15;	


	for(i=0;i<iArray_num_rows;i++)
	{
		printf("\n******Row %d******\n",(i+1));
		for(j=0;j<iArray_num_columns;j++)
		{	
			printf("\niArray[%d][%d]= %d",i,j,iArray[i][j]);

		}
		printf("\n\n");
	}

	return 0;
}