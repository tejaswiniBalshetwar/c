#include<stdio.h>

#define MAX_COLS 5
#define MAX_ROWS 3

int main(void)
{

	int iArray_2D[MAX_ROWS][MAX_COLS];
	int iArray_1D[MAX_ROWS * MAX_COLS];

	 int i,j;
	 int num;

	 printf("\n\nEnter the Elements for array : \n");
	 for(i=0;i<MAX_ROWS;i++)
	 {
	 	printf("\nEnter Number for row %d: \n",(i+1));
	 	for(j=0;j<MAX_COLS;j++)
	 	{
	 		printf("\n\nEnter Element Number: %d: \n ",(j+1));
	 		scanf("%d",&num);
	 		iArray_2D[i][j]=num;
	 	}
	 	printf("\n\n");
	 }

	 //Display 2d Array

	 for(i=0;i<MAX_ROWS;i++)
	 {
	 	printf("\nEnter Number for row %d: \n",(i+1));
	 	for(j=0;j<MAX_COLS;j++)
	 	{
	 		printf("\niArray_2D[%d][%d]=%d \n",i,j,iArray_2D[i][j]);

	 	}
	 	printf("\n\n");
	 }


	 //converting the 2D array with 

	 for(i=0;i<MAX_ROWS;i++)
	 {
	 	for(j=0;j<MAX_COLS;j++)
	 	{
	 		iArray_1D[(i*MAX_COLS)+j]=iArray_2D[i][j];
	 	}
	 }


	 //printing the 1DArray
	 printf("\n\n");
	 for(i=0;i<MAX_COLS*MAX_ROWS;i++)
	 {
	 	printf("iArray_1D[%d]:%d \n",i,iArray_1D[i]);
	 }





	return 0;
}