#include<stdio.h>
#include<stdlib.h>

int main()
{
	int **ptr=NULL;

	ptr=(int**)malloc(5*sizeof(int*));
	//ptr becomes the array of 5 int* pointers

	//now allocate memory for every int array whose address ptr each element is going to address

	//ptr[AnyNumber]=*(ptr+anyNumber)
	for(int i=0;i<5;i++)
	{
		ptr[i]=(int *)malloc(3*sizeof(int));
		if(ptr[i]==NULL)
		{
			printf("\n\nThe error while allocating the memory\n\n");
			exit(0);
		}
	}

	for(int i=0;i<5;i++)
	{
		for(int j=0;j<3;j++)
		{
			ptr[i][j]=i*j;
		}
	}

	printf("\n\n");

	printf("\n\nThe address of  ptr = base address of ptr =:%p \n",ptr);
	printf("\n\nThe address allocated to ptr are: \n");
	for(int i=0;i<5;i++)
	{
		printf("\n\n(%p+%d)=%p\n",ptr,i,(ptr+i));
		
	}

	printf("\n\nThe address of ptr &ptr[]array are:\n\n");
	for(int i=0;i<5;i++)
	{
		printf("\nptr[%d]=%p",i,&(ptr[i]));
		
	}

	printf("\n\nThe address of ptr[] pointed array are:\n\n");
	for(int i=0;i<5;i++)
	{
		printf("\nptr[%d]=%p",i,ptr[i]);
		
	}



	printf("\n\n");
	printf("\n\nThe address of ptr[] pointed array by pointer logic are:\n\n");
	for(int i=0;i<5;i++)
	{
		printf("\n(%p+%d)=%p\n",ptr,i,*(ptr+i));
		
	}


	printf("\n\n");
	printf("\n\nThe value of ptr[][]are:\n\n");
	
	for(int i=0;i<5;i++)
	{
		for(int j=0;j<3;j++)
		{
			printf("\tptr[%d][%d]=%d",i,j,ptr[i][j]);
		}
		printf("\n");
	}


	for(int i=4;i>=0;i--)
	{
		if(ptr[i])
		{
			free(ptr[i]);
			ptr[i]=NULL;
		}
	}

	if(ptr)
	{
		free(ptr);
		ptr=NULL;

	}
	return 0;
}