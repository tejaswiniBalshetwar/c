#include<stdio.h>

int main(void)
{
	int num=10;

	int* ptr=&num;

	printf("\n\nptr=%p\n\n",ptr);
	printf("\n\naddress of &(num):%p\n\n",&num);

	printf("\n\nvalue at ptr=%d\n\n",*ptr);
	printf("\n\nvalue at *(&num): %d\n\n",*(&num));


	return 0;
}