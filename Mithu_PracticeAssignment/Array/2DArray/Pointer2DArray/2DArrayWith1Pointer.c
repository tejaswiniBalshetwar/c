#include<stdio.h>
#include<stdlib.h>
int main(void)
{

	int *arr[5];
	//arr is type of int* which can store 5 int* pointers in the array

	/*for(int i=0;i<5;i++)
	{
		a[i]=(int*)malloc(3*sizeof(int));

	}
*/

	arr[0]=(int *)malloc(3*sizeof(int));
	arr[1]=(int *)malloc(3*sizeof(int));
	arr[2]=(int *)malloc(3*sizeof(int));
	arr[3]=(int *)malloc(3*sizeof(int));
	arr[4]=(int *)malloc(3*sizeof(int));

	*arr[0]=1;
	//is same as arr[0][0]
	printf("\n\nValue at arr[0]:%p",arr[0]);
	printf("\n\nValue at address pointed by arr[0][0]=*(arr[0])=%d\n\n",*(arr[0]));

	*(arr[0]+1)=2;
	//is same as *(*(arr+0)+1)
	printf("\n\nValue at arr[0]:%p",arr[0]);
	printf("\n\naddress using & arr[0][1]:%p",&(arr[0][1]));
	printf("\n\naddress using * arr[0]+1:%p",arr[0]+1);
	printf("\n\nValue at address pointed by arr[0][1]=*(arr[0]+1)=%d\n\n",*(arr[0]+1));

	*(arr[0]+2)=3;
	printf("\n\nValue at arr[0]:%p",arr[0]);
	printf("\n\nValue at address pointed by arr[0][2]=*(arr[0]+2)=%d\n\n",*(arr[0]+2));

	//arr[0][2]=*(arr[0]+2)=*(*(arr+0)+2)
	arr[0][2]=10;
	printf("\n\nValue at arr[0]:%p",arr[0]);
	printf("\n\nValue at address pointed by arr[0][2]=*(arr[0]+2)=%d\n\n",(arr[0][2]));

// 2nd row 
	arr[1][0]=4;
	printf("\n\nValue at arr[0]:%p",arr[1]);
	printf("\n\nValue at address pointed by arr[1][0]=%d\n\n",(arr[1][0]));
	printf("\n\nValue at address pointed by *(arr[1]+0)=%d\n\n",*(arr[1]+0));

	arr[1][1]=5;
	printf("\n\nValue at arr[0]:%p",arr[1]);
	printf("\n\nValue at address pointed by arr[1][1]=%d\n\n",(arr[1][1]));
	printf("\n\nValue at address pointed by *(arr[1]+1)=%d\n\n",*(arr[1]+1));



	return 0;
}