#include<stdio.h>

#define MAX_STRING_LENGTH 512


int main()
{
	//function Prototype
	int MyStrlen(char[]);

	//variable declaration


	char strArray[10][15]={"Hello!!","Welcome","to","Real","TIME","Rendering","Batch","(2018-19-20)","of","ASTROMEDICOMP"};
	int char_size;
	int strArray_size;
	int strArray_num_elements,strArray_num_rows,strArray_num_columns;
	int strActual_num_chars=0;
	int i,j;

	//code

	printf("\n\n");

	char_size=sizeof(char);
	
	strArray_size=sizeof(strArray);
	printf("\nSize of the string Array = %d",strArray_size);

	strArray_num_rows=strArray_size/sizeof(strArray[0]);
	printf("\nThe number of rows of 2D stringArray = %d",strArray_num_rows);

	strArray_num_columns=sizeof(strArray[0])/char_size;
	printf("\n the number of columns in the string Array=%d",strArray_num_columns);	

	strArray_num_elements=strArray_num_columns*strArray_num_rows;
	printf("\nNumber of Elements in 2D String Array are= %d ",strArray_num_elements);

	for(i=0;i<strArray_num_rows;i++)
	{
		strActual_num_chars=strActual_num_chars+MyStrlen(strArray[i]);
	}

	printf("\nActual Number of Elements in the 2D array strArray: %d \n\n",strActual_num_chars);


	printf("\n\nThe Actual string is\n\n");
	printf("%s ",strArray[0]);
	printf("%s ",strArray[1]);
	printf("%s ",strArray[2]);
	printf("%s ",strArray[3]);
	printf("%s ",strArray[4]);
	printf("%s ",strArray[5]);
	printf("%s ",strArray[6]);
	printf("%s ",strArray[7]);
	printf("%s ",strArray[8]);
	printf("%s \n\n",strArray[9]);


	//accessing the perticular char
	printf("\n\nThe string array accessing char by char \n");

	for(i=0;i<strArray_num_rows;i++)
	{
		printf("\n\nString Number %d => %s\n\n",(i+1),strArray[i]);
		for(j=0;j<strArray_num_columns;j++)
		{
			printf("Charecter %d = %c\n",(j+1),strArray[i][j]);

		}
		printf("\n\n");
	}


	return 0;
}


int MyStrlen(char str[])
{
	int j;
	int string_length=0;

	for(j=0;j<MAX_STRING_LENGTH;j++)
	{
		if(str[j]=='\0')
		{
			break;
		}
		else
		{
			string_length++;
		}
	}

	return (string_length);
}