#include<stdio.h>

int main(void)
{

	int iArray[5][3]={{1,2,3},{2,3,1},{3,4,5},{4,5,6},{5,6,7}};
	int int_size;
	int iArray_size;
	int iArray_num_elements,iArray_num_rows,iArray_num_columns;



	//code

	printf("\n\n\n");
	int_size=sizeof(int);

	iArray_size=sizeof(iArray);
	printf("\n\nThe size of  2D 'int' iArray is =%d",iArray_size);

	iArray_num_rows=iArray_size/sizeof(iArray[0]);
	printf("\n\n Number of rows in the 2D array iArray= %d\n\n",iArray_num_rows);

	iArray_num_columns=sizeof(iArray[0])/int_size;
	printf("\n\nNumber of Columns in iArray= %d",iArray_num_columns);

	iArray_num_elements=iArray_num_columns*iArray_num_rows;
	printf("\n\nNmuber of elements inn the 2D array iArray=%d\n",iArray_num_elements);


	printf("\n\n");
	printf("elements In the 2D array: \n\n");

	printf("******** ROW 1 *********\n");
	printf("iArray[0][0]=%d\n",iArray[0][0]);
	printf("iArray[0][1]=%d\n",iArray[0][1]);
	printf("iArray[0][2]=%d\n",iArray[0][2]);

	printf("********  ROW 2 *********\n");
	printf("iArray[1][0]=%d\n",iArray[1][0]);
	printf("iArray[1][1]=%d\n",iArray[2][1]);
	printf("iArray[1][2]=%d\n",iArray[3][2]);
	
	printf("********  ROW 3 *********\n");
	printf("iArray[2][0]=%d\n",iArray[2][0]);
	printf("iArray[2][1]=%d\n",iArray[2][1]);
	printf("iArray[2][2]=%d\n",iArray[2][2]);
	
	printf("********  ROW 4 *********\n");
	printf("iArray[3][0]=%d\n",iArray[3][0]);
	printf("iArray[3][1]=%d\n",iArray[3][1]);
	printf("iArray[3][2]=%d\n",iArray[3][2]);
	
	printf("********  ROW 5 *********\n");
	printf("iArray[4][0]=%d\n",iArray[4][0]);
	printf("iArray[4][1]=%d\n",iArray[4][1]);
	printf("iArray[4][2]=%d\n",iArray[4][2]);
	

	printf("\n\n");

	return 0;
}