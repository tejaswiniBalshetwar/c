#include<stdio.h>

int main()
{

	int iArray[]={9,10,2,3,6,4,6,65,55,54};
	int int_size;
	int iArray_size;
	int iArray_num_elements;

	float fArray[]={1.2f,3.2f,56.6f,3.6f,2.5f,4.5f,5.2f,8.7f,2.5f,5.8f};
	int float_size;
	int fArray_size;
	int fArray_num_elements;

	char cArray[]={'T','E','J','A','S','W','I','N','I'};
	int char_size;
	int cArray_size;
	int cArray_num_elements;

	//code


	//**********iArray[]*****
	printf("\n\n\n");
	printf("In-line Initialization And Piece-meal Display Of Elements of Array 'iArray[]': \n\n");
	printf("iArray[0] (1st Element)= %d\n",iArray[0]);
	printf("iArray[1] (2st Element)= %d\n",iArray[1]);
	printf("iArray[2] (3st Element)= %d\n",iArray[2]);
	printf("iArray[3] (4st Element)= %d\n",iArray[3]);
	printf("iArray[4] (5st Element)= %d\n",iArray[4]);
	printf("iArray[5] (6st Element)= %d\n",iArray[5]);
	printf("iArray[6] (7st Element)= %d\n",iArray[6]);
	printf("iArray[7] (8st Element)= %d\n",iArray[7]);
	printf("iArray[8] (9st Element)= %d\n",iArray[8]);
	printf("iArray[9] (10st Element)= %d\n\n\n",iArray[9]);

	
	int_size=sizeof(int);
	iArray_size=sizeof(iArray);
	iArray_num_elements=iArray_size/int_size;

	printf("\nsize of Data type 'int'							=%d Bytes\n\n",int_size);

	printf("\nNumber of Elements in 'int' Array 'iArray'		=%d Elements\n",iArray_num_elements);
	printf("\nsize of Array 'iArray[]'(%d Elements *%d Bytes)'	=%d Bytes\n\n",iArray_num_elements,int_size,iArray_size);




	//Float Array
	printf("\n\n\n");
	printf("In-line Initialization And Piece-meal Display Of Elements of Array 'fArray[]': \n\n");
	printf("fArray[0] (1st Element)= %f\n",fArray[0]);
	printf("fArray[1] (2st Element)= %f\n",fArray[1]);
	printf("fArray[2] (3st Element)= %f\n",fArray[2]);
	printf("fArray[3] (4st Element)= %f\n",fArray[3]);
	printf("fArray[4] (5st Element)= %f\n",fArray[4]);
	printf("fArray[5] (6st Element)= %f\n",fArray[5]);
	printf("fArray[6] (7st Element)= %f\n",fArray[6]);
	printf("fArray[7] (8st Element)= %f\n",fArray[7]);
	printf("fArray[8] (9st Element)= %f\n",fArray[8]);
	printf("fArray[9] (10st Element)= %f\n\n\n",fArray[9]);

	float_size=sizeof(float);
	fArray_size=sizeof(fArray);
	fArray_num_elements=fArray_size/float_size;

	printf("\nsize of Data type 'float'							=%d Bytes\n\n",float_size);

	printf("\nNumber of Elements in 'float' Array 'fArray'		=%d Elements\n",fArray_num_elements);
	printf("\nsize of Array 'fArray[]'(%d Elements *%d Bytes)'	=%d Bytes\n\n",fArray_num_elements,float_size,fArray_size);


	
	//******Charecter Array
	printf("\n\n\n");
	printf("In-line Initialization And Piece-meal Display Of Elements of Array 'cArray[]': \n\n");
	printf("cArray[0] (1st Element)= %c\n",cArray[0]);
	printf("cArray[1] (2st Element)= %c\n",cArray[1]);
	printf("cArray[2] (3st Element)= %c\n",cArray[2]);
	printf("cArray[3] (4st Element)= %c\n",cArray[3]);
	printf("cArray[4] (5st Element)= %c\n",cArray[4]);
	printf("cArray[5] (6st Element)= %c\n",cArray[5]);
	printf("cArray[6] (7st Element)= %c\n",cArray[6]);
	printf("cArray[7] (8st Element)= %c\n",cArray[7]);
	printf("cArray[8] (9st Element)= %c\n\n",cArray[8]);
	

	char_size=sizeof(char);
	cArray_size=sizeof(cArray);
	cArray_num_elements=cArray_size/char_size;

	printf("\nsize of Data type 'char'							=%d Bytes\n\n",char_size);

	printf("\nNumber of Elements in 'char' Array 'cArray'		=%d Elements\n",cArray_num_elements);
	printf("\nsize of Array 'cArray[]'(%d Elements *%d Bytes)'	=%d Bytes\n\n",char_size,cArray_num_elements,cArray_size);


	return 0;
}