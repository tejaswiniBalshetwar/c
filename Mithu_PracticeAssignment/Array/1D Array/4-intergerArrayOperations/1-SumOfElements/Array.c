#include<stdio.h>

#define INT_ARRAY_LENGTH 10

int main(void)
{
	int i;
	int iArray[INT_ARRAY_LENGTH];
	int sum=0;

	printf("\n\nEnter Numbers in 'iArray': \n\n");
	for(i=0;i<INT_ARRAY_LENGTH;i++)
	{
		scanf("%d",&iArray[i]);
	}

	for(i=0;i<INT_ARRAY_LENGTH;i++)
	{
		sum=sum+iArray[i];
	}

	printf("\n\nthe Elements you entered are: \n\n");
	for(i=0;i<INT_ARRAY_LENGTH;i++)
	{
		printf("%d\n",iArray[i]);
	}	

	printf("\n\nThe Sum of Elements of Array 'iArray' is = %d\n\n",sum);


	return 0;
}