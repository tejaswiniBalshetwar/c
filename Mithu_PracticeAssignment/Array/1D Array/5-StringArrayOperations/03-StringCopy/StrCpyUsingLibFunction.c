#include<stdio.h>
#include<string.h>
#define MAX_STRING_LENGTH 512

int main(void)
{
	char cArray[MAX_STRING_LENGTH],cArray_copy[MAX_STRING_LENGTH];
	int len;

	//code
	printf("\n\n");
	printf("Enter the string: ");
	gets(cArray,MAX_STRING_LENGTH);

	strcpy(cArray_copy,cArray);

	printf("\n\n");
	printf("\n The String entered by you is: %s\n",cArray);

	printf("\n\n");
	printf("\n The String copied is: %s\n",cArray_copy);

	return 0;
}

