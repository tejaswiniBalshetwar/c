#include<stdio.h>
#include<string.h>
#define MAX_STRING_LENGTH 512

int main(void)
{
	void MyStrcpy(char [], char []);
	char cArray[MAX_STRING_LENGTH],cArray_copy[MAX_STRING_LENGTH];
	int len;

	//code
	printf("\n\n");
	printf("Enter the string: ");
	gets(cArray,MAX_STRING_LENGTH);

	MyStrcpy(cArray_copy,cArray);

	printf("\n\n");
	printf("\n The String entered by you is: %s\n",cArray);

	printf("\n\n");
	MyStrcpy(cArray_copy,cArray);
	printf("\n The String copied is: %s\n",cArray_copy);

	return 0;
}


void MyStrcpy(char str_destination[],char str_source[])
{
	int MyStrlen(char []);

	int j=0;
	int string_length=MyStrlen(str_source);
	for(j=0;j<string_length;j++)
	{
		str_destination[j]=str_source[j];

	}

	str_destination[j]='\0';
} 


int MyStrlen(char SourceString[])
{
	int j=0;
	int string_length=0;

	for(j=0;j<MAX_STRING_LENGTH;j++)
	{
		if(SourceString[j]=='\0')
			break;

		string_length++;
	}

	return (string_length);
}



