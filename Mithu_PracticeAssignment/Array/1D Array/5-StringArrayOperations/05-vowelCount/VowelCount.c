#include<stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	int MyStrLen(char[]);
	char cArray[MAX_STRING_LENGTH];

	int iStringLength;

	int count_A=0,count_E=0,count_I=0,count_O=0,count_U=0;

	int i=0;

	printf("\n\n");
	printf("\nEnter The string :\n");
	gets(cArray,MAX_STRING_LENGTH);

	printf("\n\nThe String You entered is =%s\n\n",cArray);

	for(i=0;i<MyStrLen(cArray);i++)
	{
		switch(cArray[i])
		{
			case 'a':
			case 'A':
				count_A++;
				break;
			
			case 'e':
			case 'E':
				count_E++;
				break;


			case 'i':
			case 'I':
				count_I++;
				break;
		
			case 'o':
			case 'O':
				count_O++;
				break;

			case 'u':
			case 'U':
				count_U++;
				break;
		}
	}


	printf("\n\n'A' in the string is=%d",count_A);
	printf("\n\n'E' in the string is=%d",count_E);
	
	printf("\n\n'I' in the string is=%d",count_I);
	
	printf("\n\n'O' in the string is=%d",count_O);
	printf("\n\n'U' in the string is=%d",count_U);
	
	printf("\n\n");
	return 0;
}


int MyStrLen(char SourceStr[])
{
	int j=0;
	int String_length=0;

	for(j=0;j<MAX_STRING_LENGTH;j++)
	{
		if(SourceStr[j]=='\0')
			break;

		String_length++;
	}

	return String_length;
}
