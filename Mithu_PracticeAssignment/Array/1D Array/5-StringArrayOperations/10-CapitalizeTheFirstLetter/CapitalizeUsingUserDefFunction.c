#include<stdio.h>
#include<string.h>
#include<ctype.h>



#define MAX_STRING_LENGTH 512

int main(void)
{
	int MyStrLen(char[]);
	int MyToUpper(char );
	char cArray[MAX_STRING_LENGTH],cArray_CapitalizeFirstLetter[MAX_STRING_LENGTH];
	int i,j;


	//code
	printf("\n\n");
	printf("Enter the 1st string: ");
	gets(cArray,MAX_STRING_LENGTH);


	printf("\n\n");
	printf("\n The String entered by you is: %s\n",cArray);


	printf("\n\n");
	j=0;
	for(i=0;i<MyStrLen(cArray);i++)
	{
		if(i==0)
		{
			cArray_CapitalizeFirstLetter[j]=MyToUpper(cArray[i]);
		}
		else if(cArray[i]==' ')
		{
			cArray_CapitalizeFirstLetter[j]=cArray[i];
			cArray_CapitalizeFirstLetter[j+1]=MyToUpper(cArray[i+1]);
			i++;
			j++;
		}
		else if((cArray[i]=='.'||cArray[i]=='?'||cArray[i]==','||cArray[i]=='!')&&cArray[i]!=' ')
		{
			cArray_CapitalizeFirstLetter[j]=cArray[i];
			cArray_CapitalizeFirstLetter[j+1]=' ';
			cArray_CapitalizeFirstLetter[j+2]=MyToUpper(cArray[i+1]);
			i++;
			j=j+2;
		}
		else
		{
			cArray_CapitalizeFirstLetter[j]=cArray[i];
		}
		j++;
	}

	cArray_CapitalizeFirstLetter[j]='\0';
	
	printf("\n The String after capitalization: %s\n\n",cArray_CapitalizeFirstLetter);

	return 0;
}


int MyStrLen(char SourceStr[])
{
	int j=0;
	int String_length=0;

	for(j=0;j<MAX_STRING_LENGTH;j++)
	{
		if(SourceStr[j]=='\0')
			break;

		String_length++;
	}

	return String_length;
}

int MyToUpper(char ch)
{

	int num='a'-'A';
	int c=0;
	if(((int)ch>=97) && ((int)ch<=122))
	{
		c=(int)ch-num;
		return ((char)c);
	}
	else
	{
		return(ch);
	}
}

