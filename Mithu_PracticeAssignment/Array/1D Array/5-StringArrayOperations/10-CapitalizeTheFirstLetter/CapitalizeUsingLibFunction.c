#include<stdio.h>
#include<string.h>
#include<ctype.h>



#define MAX_STRING_LENGTH 512

int main(void)
{
	int MyStrLen(char[]);
	char cArray[MAX_STRING_LENGTH],cArray_CapitalizeFirstLetter[MAX_STRING_LENGTH];
	int i,j;


	//code
	printf("\n\n");
	printf("Enter the 1st string: ");
	gets(cArray,MAX_STRING_LENGTH);


	printf("\n\n");
	printf("\n The String entered by you is: %s\n",cArray);


	printf("\n\n");
	j=0;
	for(i=0;i<MyStrLen(cArray);i++)
	{
		if(i==0)
		{
			cArray_CapitalizeFirstLetter[j]=toupper(cArray[i]);
		}
		else if(cArray[i]==' ')
		{
			cArray_CapitalizeFirstLetter[j]=toupper(cArray[i]);
			cArray_CapitalizeFirstLetter[j+1]=toupper(cArray[i+1]);
			i++;
			j++;
		}
		else
		{
			cArray_CapitalizeFirstLetter[j]=cArray[i];
		}
		j++;
	}

	cArray_CapitalizeFirstLetter[j]='\0';
	
	printf("\n The String after capitalization: %s\n\n",cArray_CapitalizeFirstLetter);

	return 0;
}


int MyStrLen(char SourceStr[])
{
	int j=0;
	int String_length=0;

	for(j=0;j<MAX_STRING_LENGTH;j++)
	{
		if(SourceStr[j]=='\0')
			break;

		String_length++;
	}

	return String_length;
}

