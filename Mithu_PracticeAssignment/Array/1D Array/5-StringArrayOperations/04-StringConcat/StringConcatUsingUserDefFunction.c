
#include<stdio.h>
#include<string.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	void MyStrConcat(char[],char[]);
	char cArray[MAX_STRING_LENGTH],cArray_Concat[MAX_STRING_LENGTH];
	int len;


	//code
	printf("\n\n");
	printf("Enter the 1st string: ");
	gets(cArray,MAX_STRING_LENGTH);

	printf("\n\n");
	printf("Enter the 2nd string: ");
	gets(cArray_Concat,MAX_STRING_LENGTH);


	printf("\n\n");
	printf("\n The String 1st entered by you is: %s\n",cArray);


	printf("\n\n");
	printf("\n The 2nd String entered by you is: %s\n",cArray_Concat);

	printf("\n\n");
	printf("\nAfter String Concated :\n\n");


	MyStrConcat(cArray,cArray_Concat);
	printf("\n\n");
	printf("\n The 1st String entered by you is: %s\n",cArray);


	printf("\n\n");
	printf("\n The 2nd String entered by you is: %s\n",cArray_Concat);

	return 0;
}

void MyStrConcat(char SourceStr[],char DestinationStr[])
{
	int MyStrLen(char[]);

	int j=0;
	int String_length1=MyStrLen(SourceStr);
	int String_length2=MyStrLen(DestinationStr);
	int i;
	
	i=0;
	for(j=String_length1;j<(String_length2+String_length1),SourceStr[i]!='\0';j++,i++)
	{	

		SourceStr[j]=DestinationStr[i];
	}

	DestinationStr[j]='\0';

}

int MyStrLen(char SourceStr[])
{
	int j=0;
	int String_length=0;

	for(j=0;j<MAX_STRING_LENGTH;j++)
	{
		if(SourceStr[j]=='\0')
			break;

		String_length++;
	}

	return String_length;
}

