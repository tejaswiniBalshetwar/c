#include<stdio.h>

int main(void)
{
	char cArray_01[]={'T','E','J','S','W','I','N','I','\0'};
	char cArray_02[14]={'A','S','T','R','O','M','E','D','I','C','O','M','P','\0'};
	char cArray_03[]={'H','E','L','L','O','\0'};
	char cArray_04[]="TO";
	char cArray_05[]="All of you at Real Time Rendering batch 18-19-2020";

	char cArray_WithoutNullTerminator[]={'W','E','L','C','O','M','E'};

	printf("\n\n");
	printf("size of cArray_01:%d\n",sizeof(cArray_01));
	printf("size of cArray_02:%d\n",sizeof(cArray_02));

	printf("size of cArray_03:%d\n",sizeof(cArray_03));
	printf("size of cArray_04:%d\n",sizeof(cArray_04));
	printf("size of cArray_05:%d\n",sizeof(cArray_05));

	printf("\n\n\n");
	printf("The Strings are: \n\n");
	printf("%s\n", cArray_01);\
	printf("%s\n", cArray_02);
	printf("%s\n", cArray_03);
	printf("%s\n", cArray_04);
	printf("%s\n", cArray_05);

	printf("\n\n");
	printf("size of char array without null termination: %d\n",sizeof(cArray_WithoutNullTerminator) );
	printf("char array without null termination:%s\n",cArray_WithoutNullTerminator);

	return 0;
}