
#include<stdio.h>
#include<string.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	void MyStrRev(char[],char[]);
	char cArray[MAX_STRING_LENGTH],cArray_Reverse[MAX_STRING_LENGTH];
	int len;

	//code
	printf("\n\n");
	printf("Enter the string: ");
	gets(cArray,MAX_STRING_LENGTH);

	printf("\n\n");
	printf("\n The String entered by you is: %s\n",cArray);

	printf("\n\n");
	printf("\nThe String Reversed is:\n\n");
	MyStrRev(cArray,cArray_Reverse);
	printf("%s\n",cArray_Reverse);

	return 0;
}

void MyStrRev(char SourceStr[],char DestinationStr[])
{
	int MyStrLen(char[]);

	int j=0;
	int String_length=MyStrLen(SourceStr);
	int i;
	
	i=String_length-1;
	for(j=0;j<String_length,SourceStr[i]!='\0';j++,i--)
	{	

		DestinationStr[j]=SourceStr[i];
	}

	DestinationStr[j]='\0';

}

int MyStrLen(char SourceStr[])
{
	int j=0;
	int String_length=0;

	for(j=0;j<MAX_STRING_LENGTH;j++)
	{
		if(SourceStr[j]=='\0')
			break;

		String_length++;
	}

	return String_length;
}

