#include<stdio.h>

int main()
{

	int iArray[]={9,10,2,3,6,4,6,65,55,54};
	int int_size;
	int iArray_size;
	int iArray_num_elements;

	float fArray[]={1.2f,3.2f,56.6f,3.6f,2.5f,4.5f,5.2f,8.7f,2.5f,5.8f};
	int float_size;
	int fArray_size;
	int fArray_num_elements;

	char cArray[]={'T','E','J','A','S','W','I','N','I'};
	int char_size;
	int cArray_size;
	int cArray_num_elements;


	int i;
	//code


	//**********iArray[]*****
	printf("\n\n\n");
	printf("In-line Initialization And Loop (for) Display Of Elements of Array 'iArray[]': \n\n");
	
	
	int_size=sizeof(int);
	iArray_size=sizeof(iArray);
	iArray_num_elements=iArray_size/int_size;

	for(i=0;i<iArray_num_elements;i++)
	{
		printf("iArray[%d] (Element  %d)	= %d\n",i,(i+1),iArray[i]);

	}

	printf("\nsize of Data type 'int'		=%d Bytes\n\n",int_size);

	printf("\nNumber of Elements in 'int' Array 'iArray'		=%d Elements\n",iArray_num_elements);
	printf("\nsize of Array 'iArray[]'(%d Elements *%d Bytes)'		=%d Bytes\n\n",iArray_num_elements,int_size,iArray_size);


	//************fArray*********
	printf("\n\n\n");
	printf("In-line Initialization And Loop (while) Display Of Elements of Array 'fArray[]': \n\n");
	
	float_size=sizeof(float);
	fArray_size=sizeof(fArray);
	fArray_num_elements=fArray_size/float_size;

	i=0;

	while(i<fArray_num_elements)
	{
		printf("fArray[%d] (Element %d) =%f\n",i,(i+1),fArray[i]);
		i++;
	}

	printf("\nsize of Data type 'float'							=%d Bytes\n\n",float_size);

	printf("\nNumber of Elements in 'float' Array 'fArray'		=%d Elements\n",fArray_num_elements);
	printf("\nsize of Array 'fArray[]'(%d Elements *%d Bytes)'	=%d Bytes\n\n",fArray_num_elements,float_size,fArray_size);



	//charecter Array
	printf("\n\n\n");
	printf("In-line Initialization And Loop (do while) Display Of Elements of Array 'cArray[]': \n\n");
	

	char_size=sizeof(char);
	cArray_size=sizeof(cArray);
	cArray_num_elements=cArray_size/char_size;

	i=0;
	do
	{	
		printf("cArray[%d] (Element %d)= %c\n",i,(i+1),cArray[i]);
		i++;

	}while(i<cArray_num_elements);
	

	printf("\nsize of Data type 'char'							=%d Bytes\n\n",char_size);

	printf("\nNumber of Elements in 'char' Array 'cArray'		=%d Elements\n",cArray_num_elements);
	printf("\nsize of Array 'cArray[]'(%d Elements *%d Bytes)'	=%d Bytes\n\n",char_size,cArray_num_elements,cArray_size);


	return 0;
}