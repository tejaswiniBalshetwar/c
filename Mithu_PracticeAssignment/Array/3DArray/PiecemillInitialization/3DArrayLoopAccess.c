#include<stdio.h>

int main(void)
{
	int iArray[5][3][2]={
							{{1,2},{3,4},{1,3}},
							{{2,4},{5,5},{8,2}},
							{{2,5},{5,8},{7,9}},
							{{10,20},{11,12},{3,5}},
							{{5,6},{7,9},{4,6}}
						};


   int int_size;
   int iArray_Size;
   int iArray_num_elements,iArray_width,iArray_height,iArray_deapth;

   printf("\n\n");
   int_size=sizeof(int);
   iArray_Size=sizeof(iArray);
   printf("Size of three Dimension(3D) array:=%d\n\n",iArray_Size);

   iArray_width=iArray_Size/sizeof(iArray[0]);
   printf("width of three Dimension(3D) array:=%d\n\n",iArray_width);

   iArray_height=sizeof(iArray[0])/sizeof(iArray[0][0]);
   printf("height of three Dimension(3D) array:=%d\n\n",iArray_height);

   iArray_deapth=sizeof(iArray[0][0])/sizeof(int);
   printf("width of three Dimension(3D) array:=%d\n\n",iArray_width);

   iArray_num_elements=iArray_width* iArray_height*iArray_deapth;
   printf("Number of elements in three dimesional array are= %d\n\n",iArray_num_elements );

   printf("\n\n elements in Integer 3D array are : \n\n");

   for(int i=0;i<iArray_width;i++)
   {
      printf("****ROW %d******",(i+1));
      for(int j=0;j<iArray_height;j++)
      {
         printf("******COLUMN %d******\n",(j+1));
         for(int k=0;k<iArray_deapth;k++)
         {
            printf("iArray [%d][%d][%d]= %d \n",i,j,k,iArray[i][j][k]);
         }
         printf("\n");
      }
      printf("\n\n");
   }

   return (0);
}