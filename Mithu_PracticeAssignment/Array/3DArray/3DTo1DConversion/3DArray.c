#include<stdio.h>

#define NUM_ROWS 5
#define NUM_COLUMNS 3
#define DEPTH 2

int main(void)
{
	//variable declarations
	int iArray[NUM_ROWS][NUM_COLUMNS][DEPTH]={
												{{9,18},{27,35},{45,46}},
												{{10,20},{30,40},{50,60}},
												{{7,14},{21,28},{35,42}},
												{{6,12},{18,24},{30,42}},
												{{5,10},{15,20},{25,30}},
											 };
	int i,j,k;
	int iArray_1D[NUM_ROWS * NUM_COLUMNS *DEPTH];

	//code

	printf("\n\n");
	printf("Elements In the 3D array:\n\n");
	for(i=0;i<NUM_ROWS;i++)
	{
		printf("**********ROW %d *******\n",(i+1));
		for(j=0;j<NUM_COLUMNS;j++)
		{
			printf("********COLUMN %d *******\n",(j+1));
			for(k=0;k<DEPTH;k++)
			{
				printf("iArray[%d][%d][%d]=%d\n",i,j,k,iArray[i][j][k]);
			}
			printf("\n");
		}
		printf("\n\n");
	}


	//converting 3D to 1D
	for(i=0;i<NUM_ROWS;i++)
	{
		for(j=0;j<NUM_COLUMNS;j++)
		{
			for(k=0;k<DEPTH;k++)
			{
				iArray_1D[(i*NUM_COLUMNS*DEPTH)+(j*DEPTH)+k]=iArray[i][j][k];
			}
		}
	}

	printf("\n\n");
	printf("Elements In the 3D array:\n\n");

	for(i=0;i<(NUM_COLUMNS*NUM_ROWS*DEPTH);i++)
	{
		printf("iArray_1D[%d]=%d\n",i,iArray_1D[i]);
	}

	return 0;
}