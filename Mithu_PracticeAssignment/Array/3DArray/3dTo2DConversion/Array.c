#include<stdio.h>

#define NUM_ROWS 5
#define NUM_COLUMNS 3
#define DEPTH 2

int main(void)
{

	//variable declarations

	int iArray[NUM_ROWS][NUM_COLUMNS][DEPTH]={
												{{9,18},{27,35},{45,46}},
												{{20,10},{30,40},{50,69}},
												{{7,14},{21,28},{35,42}},
												{{6,12},{18,24},{30,42}},
												{{5,10},{15,20},{25,30}},
											 };
	int i,j,k;
	int iArray_2D[NUM_ROWS][NUM_COLUMNS*DEPTH];

	//code

	//display 3D array
	printf("\n\n");
	printf("Elements In the Array: \n\n");
	for(i=0;i<NUM_ROWS;i++)
	{
		printf("**********ROW %d *******\n",(i+1));
		for(j=0;j<NUM_COLUMNS;j++)
		{
			printf("********COLUMN %d *******\n",(j+1));
			for(k=0;k<DEPTH;k++)
			{
				printf("iArray[%d][%d][%d]=%d\n",i,j,k,iArray[i][j][k]);
			}
			printf("\n");
		}
		printf("\n\n");
	}


	//**********CONVERTING 3D TO 2D ********
	for(i=0;i<NUM_ROWS;i++)
	{
		for(j=0;j<NUM_COLUMNS;j++)
		{
			for(k=0;k<DEPTH;k++)
			{
				iArray_2D[i][(j*DEPTH)+k]=iArray[i][j][k];
			}
		}
	}


	//DISPLAY 2D ARRAY
	printf("\n\n");
	printf("Elements in the 2D array: \n\n");

	for(i=0;i<NUM_ROWS;i++)
	{
		printf("*******ROW %d **********\n",(i+1));
		for(j=0;j<NUM_COLUMNS*DEPTH;j++)
		{	
			printf("iArray_2D[%d][%d]=%d \n",i,j,iArray_2D[i][j]);
		}
		printf("\n");
	}

	return 0;
}