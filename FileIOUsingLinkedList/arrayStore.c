#include<stdio.h>
//#include<malloc.h>
#include<stdlib.h>
#include<string.h>

#define SIZE 30
#define LENGTH 100


FILE *pFile=NULL;
FILE *pFileForStore=NULL;

struct fileData
{
	char *inputString;
	float **vertices;
	struct fileData *next;
};

struct fileData *head=NULL;

int main(int argc,char ** argv)
{
	char *fileName=NULL;
	void insertNode(char *);
	void displayList();
	void readFile();
	void storeDataInFile();
	fileName=(char *)malloc(SIZE * sizeof(char));
	int choice;
	while(1)
	{
		printf("\n\n\nPress 1 to continue to enter file name 0 to exit:\n");
		scanf("%d",&choice);

		if(choice==0)
		{
			printf("\nExiting the code\n\n");
			break;
		}
		else
		{
			printf("\n\nEnter the name of file to Read: ");
			scanf("%s",fileName);
			//gets(&fileName);

			printf("\n\n%s\n\n",fileName);

			pFile=fopen(fileName,"r");
			if(pFile==NULL)
			{
				printf("\n\n\nError While Opening file!!!!!!\n\n ");
				exit(0);
			}

			readFile();
			while(choice!=3)
			{
				printf("\nselect\n 1-to show output on screen : \n 2-to store output in file:\n 3-to re-enter the file name:\n ");
				scanf("%d",&choice);
				switch(choice)
				{
					case 1:
						displayList();
						break;
					case 2:
						storeDataInFile();
						break;
					case 3:
						printf("\n\nExiting the Menu \n\n");
						break;
				}
				
				
			}
		}
		
	}

	return 0;
}


void readFile()
{

	//function declaration 
	void insertNode(char *);

	char *tempString=(char *)malloc(LENGTH * sizeof(char));
	char *tokenString=NULL;
	int i=0;

	while(!feof(pFile))
	{
		//printf("\n\n %d",feof(pFile));
		fgets(tempString,LENGTH,pFile);
		printf("\n\n%s\n",tempString);
		insertNode(tempString);		
	}
}


void insertNode(char *insertData)
{
	char *tokenString=NULL;
	char *tokenStringForIndividualVertices=NULL;
	struct fileData *node=(struct fileData*)malloc(sizeof(struct fileData)); 
	
	printf("\n\ninsertData:%s\n\n",insertData);
	tokenString=strtok(insertData," ");
	int i=0;
	int j=0;
	int k=0;
	if(head==NULL)
	{
		if(!strcmp(tokenString,"v")||!strcmp(tokenString,"vn")||!strcmp(tokenString,"vt"))
		{
			head=(struct fileData*)malloc(sizeof(struct fileData));
			head->inputString=(char*)malloc(4*sizeof(char));

			//*head->inputString=*tokenString;
			while(*(tokenString+j)!='\0')
			{
				*(head->inputString+j)=*(tokenString+j);
				j++;

			}
			printf("\ncharecter 1 HEAD tokenString:%s\n",head->inputString);
			
			head->vertices=(float**)malloc(3*sizeof(float*));
			while(tokenString!=NULL)
			{
				tokenString=strtok(NULL," ");
				*(head->vertices+i)=(float*)malloc(13*sizeof(float));
				if(tokenString){
					*((*head->vertices)+i)=atof(tokenString);
					printf("\ncharecter 6 at i=%d tokenString:%f\n",i,*((*head->vertices)+i));
					i=i+1;
				}
			}
			head->next=NULL;			
		}
		else
		{
			head=(struct fileData*)malloc(sizeof(struct fileData));
			head->inputString=(char *)malloc(3*sizeof(char));

			while(*(tokenString+j)!='\0')
			{
				*(head->inputString+j)=*(tokenString+j);
				j++;
			}


			while(tokenString!=NULL)
			{
				tokenStringForIndividualVertices=strtok(NULL," ");
				head->vertices=(float**)malloc(3*sizeof(float*));
				while(tokenStringForIndividualVertices!=NULL)
				{
					tokenStringForIndividualVertices=strtok(NULL,"/");

					if(tokenStringForIndividualVertices)
					{
						*(head->vertices+i)=(float*)malloc(15*sizeof(float));
						*(*(head->vertices+i)+k)=atof(tokenStringForIndividualVertices);
						k++;
						printf("input value=%f",*(*(head->vertices+i)+k));
					}

				}
				i++;

			}
		}
		
	}
	else
	{
		if(!strcmp(tokenString,"v")||!strcmp(tokenString,"vn")||!strcmp(tokenString,"vt"))
		{
			node=(struct fileData*)malloc(sizeof(struct fileData));
			node->inputString=(char*)malloc(4*sizeof(char));

			while(*(tokenString+j)!='\0')
			{
				*(node->inputString+j)=*(tokenString+j);
				j++;

			}
			printf("\ncharecter 1 node tokenString:%s\n",node->inputString);
			
			node->vertices=(float**)malloc(3*sizeof(float*));
			while(tokenString!=NULL)
			{
				tokenString=strtok(NULL," ");
				*(node->vertices+i)=(float*)malloc(13*sizeof(float));
				if(tokenString){
					*((*node->vertices)+i)=atof(tokenString);
					printf("\ncharecter 6 at i=%d tokenString:%f\n",i,*(*node->vertices+i));
					i=i+1;
				}
			}
			node->next=head;
			head=node;			
		}

		else
		{
			node=(struct fileData*)malloc(sizeof(struct fileData));
			node->inputString=(char *)malloc(3*sizeof(char));

			printf("\n\ninput tokenString =%s",tokenString);
			while(*(tokenString+j)!='\0')
			{
				*(node->inputString+j)=*(tokenString+j);
				j++;
			}


			while(tokenString!=NULL)
			{
				tokenString=strtok(NULL," ");
				printf("\n\ninput tokenString by 1: space=%s",tokenString);

				/*if(tokenString)
				{
					tokenStringForIndividualVertices=strtok(tokenString,"/");
					
					printf("\n\ninput tokenString by 0: space=%s",tokenString);
					printf("\n\ninput tokenString by 2:==>/=%s",tokenStringForIndividualVertices);

					//node->vertices=(float**)malloc(3*sizeof(float*));
					while(tokenStringForIndividualVertices!=NULL)
					{
						tokenStringForIndividualVertices=strtok(NULL,"/");
						printf("\n\ninput tokenStringForIndividualVertices 3: by /=%s",tokenStringForIndividualVertices);
						*//*if(tokenStringForIndividualVertices)
						{
							*(node->vertices+i)=(float*)malloc(15*sizeof(float));
							*(*(node->vertices+i)+k)=atoi(tokenStringForIndividualVertices);
							k++;
							printf("\ninput value=%f",*(*(head->vertices+i)+k));
						
						}*/
					/*}
					i++;*/
				//printf("\n\ninput tokenString by 1: space=%s",tokenString);
				//}

			}
			node->next=head;
			head=node;		
		}

	}

}

void displayList()
{
	struct fileData* node=(struct fileData*)malloc(sizeof(struct fileData));
	node=head;
	while(node!=NULL)
	{
		printf("\n\nThe Value of inputString:%s",(node->inputString));
		for(int i=0;i<3;i++)
		{
			printf("\t%f",*(*node->vertices+i));
		}
		node=node->next;
		printf("\n");
	}
}

void storeDataInFile()
{
	struct fileData* node=(struct fileData*)malloc(sizeof(struct fileData));
	pFileForStore=fopen("log.txt","w+");

	if(pFileForStore==NULL)
	{
		printf("\nerror while opening file\n");
		exit(0);
	}

	node=head;
	while(node!=NULL)
	{
		fprintf(pFileForStore,"\n\nThe Value of inputString:%s",(node->inputString));
		for(int i=0;i<3;i++)
		{
			fprintf(pFileForStore,"\t%f",*(*node->vertices+i));
		}
		node=node->next;
		fprintf(pFileForStore,"\n");
	}
}