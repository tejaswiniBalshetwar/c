#include<stdio.h>
//#include<malloc.h>
#include<stdlib.h>
#include<string.h>

#define SIZE 30
#define LENGTH 100


FILE *pFile=NULL;

struct fileData
{
	char *inputString;
	float *vertices;
	struct fileData *next;
};

struct fileData *head=NULL;

int main(int argc,char ** argv)
{
	char *fileName=NULL;
	void insertNode(char *);
	void displayList();
	void readFile();


	fileName=(char *)malloc(SIZE * sizeof(char));
	printf("\n\nEnter the name of file to Read: ");
	scanf("%s",fileName);
	//gets(&fileName);

	printf("\n\n%s\n\n",fileName);

	pFile=fopen(fileName,"r");
	if(pFile==NULL)
	{
		printf("\n\n\nError While Opening file!!!!!!\n\n ");
		exit(0);
	}

	readFile();
	displayList();

	return 0;
}


void readFile()
{

	//function declaration 
	void insertNode(char *);

	char *tempString=(char *)malloc(LENGTH * sizeof(char));
	char *tokenString=NULL;
	int i=0;

	while(!feof(pFile))
	{
		//printf("\n\n %d",feof(pFile));
		fgets(tempString,LENGTH,pFile);
		printf("\n\n%s\n",tempString);
		tokenString=strtok(tempString," ");
		if(!strcmp(tokenString,"v")||!strcmp(tokenString,"vn")||!strcmp(tokenString,"vt"))
		{
			insertNode(tokenString);
			while(tokenString!=NULL && tokenString!="\\n")
			{
				//printf("\n\ntokenString1:%s\n\n",tokenString);	
				tokenString=strtok(NULL," ");
				insertNode(tokenString);
				//printf("\n\ntokenString2:%s\n\n",tokenString);
			}	
		}
		
	}
}


void insertNode(char *insertData)
{
	printf("\n\ninsertData:%s\n\n",insertData);
	if(insertData)
	{

		if(head==NULL)
		{
			printf("\n\nafter if:");
			head=(struct fileData*)malloc(sizeof(struct fileData));
			head->vertices=(float*)malloc(13*sizeof(float));
			*head->vertices=atof(insertData);
			head->next=NULL;
			printf("\n\nhead value%f",*head->vertices);
			
			//head=tempNode;
			//printf("\n\nafter if:%f\n\n",*head->vertices);
		}
		else
		{
			printf("\n\nafter else:");
			struct fileData *tempNode=(struct fileData*)malloc(sizeof(struct fileData));
			tempNode->vertices=(float*)malloc(13*sizeof(float));
			*tempNode->vertices=atof(insertData);
			printf("\n\nTempNode value%f",*tempNode->vertices);
			tempNode->next=head;
			//head->next=tempNode;
			head=tempNode;
		}
	
	}	
}

void displayList()
{
	struct fileData* node=(struct fileData*)malloc(sizeof(struct fileData));
	node=head;
	while(node!=NULL)
	{
		printf("\n\nThe Value of vertices:%f\n",*node->vertices);
		node=node->next;
	}
}